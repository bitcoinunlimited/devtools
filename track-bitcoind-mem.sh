#!/bin/bash

if [ $# -lt 1 ]; then
    echo "usage: ./track-bitcoind-mem.sh <name of output file>"
    echo "example: ./track-bitcoind-mem.sh dev_tip.txt"
    exit
fi

while true; do
    ps --pid=`pidof bitcoind` -o pid,user,%mem,rss,command >> "$1";
    sleep 5;
done
