#!/usr/bin/env python3
import sys
import os
import os.path
import hashlib
import pprint

def main(argv):
  if len(argv) < 2:
    print ("Usage: sign.py [version] [program name]")
    print ("Example: sign.py 1.9.2.0 BCHunlimited")
    return -1
  
  d = os.path.dirname(os.path.realpath(__file__))
  d = d + "/releases/" + str(argv[1]) + "/all"

  files = os.listdir(d)
  sha = {}
  for f in files:
    if "sign_" in f and ".json" in f:  # Don't sign an old copy of this signing file
      continue
    if "-unsigned" in f:
      old = f
      f = f.replace("-unsigned","")
      os.rename(d + os.sep + old,d + os.sep + f)
    fp = open(d + os.sep + f,"rb")
    hsh = hashlib.sha256()
    hsh.update(fp.read())
    fp.close()
    sha[f] = hsh.hexdigest()
  sig = {}

  if len(argv) <= 2: sig["version"] = "1.9.2.0"
  else: sig["version"] = argv[1]
  if len(argv) <= 3: sig["program"] = "BCHunlimited"
  else: sig["program"] = argv[2]
  sig["files"] = sha

  print (pprint.pformat(sig, indent=2,width=80))
  f = open(os.path.join(d, "sign_%s.json" % sig["version"]),"w")
  s = pprint.pformat(sig, indent=2,width=80)
  f.write(s)
  f.close()
  return sig

  
def Test():
  main(["","/me/bitcoin/releases/0.12.1b"])

if __name__ == "__main__":
  main(sys.argv)
