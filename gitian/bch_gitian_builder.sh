#!/bin/bash
export VERSION=1.9.2.0
#export TAG=BCHunlimited${VERSION}
export TAG=dev
BUILD_LINUX=1
BUILD_MAC=1
BUILD_WINDOWS=1
BUILD_ARM=1
######################################################
### Do not edit anything below this line unless you know what you are doing
######################################################
unset USE_LXC
export USE_DOCKER=1
export SIGNER=

ABSOLUTE_ROOT_PATH=$(pwd)
# make the releases folder if it does not already exist
if [ ! -d "releases" ]; then
    mkdir releases
fi
# make the var folder if it does not already exist
if [ ! -d "var" ]; then
    mkdir var
fi
# make the inputs folder if it does not exist
if [ ! -d "inputs" ]; then
    mkdir inputs
    curl --location --fail https://www.bitcoinunlimited.info/sdks/MacOSX10.11.sdk.tar.gz -o ./inputs/MacOSX10.11.sdk.tar.gz
fi
# clone the build source if it does not exist
if [ ! -d "bchunlimited" ]; then
    git clone https://gitlab.com/bitcoinunlimited/bchunlimited
fi
# check for arguments to update source
if [ $# -ne 0 ]; then
    for arg in $@
    do
        if [ arg == "--pull-source"]; then
            cd bchunlimited
            git pull
            cd ..
        else
            echo "$arg is not a recognised argument"
        fi
    done
fi

export ROOT_DIR=${ABSOLUTE_ROOT_PATH}
export BCHPATH=${ROOT_DIR}/bchunlimited
export REL_DIR=${ROOT_DIR}/releases/${VERSION}
mkdir -p ${REL_DIR}
mkdir -p ${REL_DIR}/info
mkdir -p ${REL_DIR}/arm
mkdir -p ${REL_DIR}/linux
mkdir -p ${REL_DIR}/osx
mkdir -p ${REL_DIR}/win
if [ $BUILD_LINUX -eq 1 ]; then
    echo
    echo -----------------------------------------------------------------------------------------
    echo ./bin/gbuild -j 8 -m 10000 --url bchunlimited=${BCHPATH} --commit bitcoin=${TAG} ${BCHPATH}/contrib/gitian-descriptors/gitian-linux.yml
    ./bin/gbuild -j 8 -m 10000 --url bchunlimited=${BCHPATH} --commit bitcoin=${TAG} ${BCHPATH}/contrib/gitian-descriptors/gitian-linux.yml
    mkdir -p ${REL_DIR}/info/linux
    cp var/* ${REL_DIR}/info/linux
    mv build/out/* ${REL_DIR}/linux/
    mv var/build.log build_linux.log
fi
if [ $BUILD_MAC -eq 1 ]; then
    echo
    echo -----------------------------------------------------------------------------------------
    echo ./bin/gbuild -j 8 -m 10000 --url bchunlimited=${BCHPATH} --commit bitcoin=${TAG} ${BCHPATH}/contrib/gitian-descriptors/gitian-osx.yml
    ./bin/gbuild -j 8 -m 10000 --url bchunlimited=${BCHPATH} --commit bitcoin=${TAG} ${BCHPATH}/contrib/gitian-descriptors/gitian-osx.yml
    mkdir -p ${REL_DIR}/info/osx
    cp var/* ${REL_DIR}/info/osx
    mv build/out/* ${REL_DIR}/osx/
    mv var/build.log build_osx.log
fi
if [ $BUILD_WINDOWS -eq 1 ]; then
    echo
    echo -----------------------------------------------------------------------------------------
    echo ./bin/gbuild -j 8 -m 10000 --url bchunlimited=${BCHPATH} --commit bitcoin=${TAG} ${BCHPATH}/contrib/gitian-descriptors/gitian-win.yml
    ./bin/gbuild -j 8 -m 10000 --url bchunlimited=${BCHPATH} --commit bitcoin=${TAG} ${BCHPATH}/contrib/gitian-descriptors/gitian-win.yml
    mkdir -p ${REL_DIR}/info/win
    cp var/* ${REL_DIR}/info/win
    mv build/out/* ${REL_DIR}/win/
    mv var/build.log build_win.log
fi
if [ $BUILD_ARM -eq 1 ]; then
    echo
    echo -----------------------------------------------------------------------------------------
    echo ./bin/gbuild -j 8 -m 10000 --url bchunlimited=${BCHPATH} --commit bitcoin=${TAG} ${BCHPATH}/contrib/gitian-descriptors/gitian-arm.yml
    ./bin/gbuild -j 8 -m 10000 --url bchunlimited=${BCHPATH} --commit bitcoin=${TAG} ${BCHPATH}/contrib/gitian-descriptors/gitian-arm.yml
    mkdir -p ${REL_DIR}/info/arm
    cp var/* ${REL_DIR}/info/arm
    mv build/out/* ${REL_DIR}/arm/
    mv var/build.log build_arm.log
fi
BASE_STRING="gitian build complete for:"
if [ $BUILD_LINUX -eq 1 ]; then
    LINUX_STR=" linux,"
    BASE_STRING=$BASE_STRING$LINUX_STR
fi
if [ $BUILD_MAC -eq 1 ]; then
    MAC_STR=" mac,"
    BASE_STRING=$BASE_STRING$MAC_STR
fi
if [ $BUILD_WINDOWS -eq 1 ]; then
    WINDOWS_STR=" windows,"
    BASE_STRING=$BASE_STRING$WINDOWS_STR
fi
if [ $BUILD_ARM -eq 1 ]; then
    ARM_STR=" arm"
    BASE_STRING=$BASE_STRING$ARM_STR
fi
echo $BASE_STRING
echo "Gathering releases..."

export ROOT_DIR=${ABSOLUTE_ROOT_PATH}
export PFX=${ROOT_DIR}/releases
export FILE_PFX=bch-unlimited
export DIR_VERSION=${VERSION}
export FILE_VERSION=${VERSION}

mkdir -p ${PFX}/${DIR_VERSION}/all
cp ${PFX}/${DIR_VERSION}/arm/*-arm32.tar.gz ${PFX}/${DIR_VERSION}/all/${FILE_PFX}-${FILE_VERSION}-arm32.tar.gz
cp ${PFX}/${DIR_VERSION}/arm/*-arm64.tar.gz ${PFX}/${DIR_VERSION}/all/${FILE_PFX}-${FILE_VERSION}-arm64.tar.gz

cp ${PFX}/${DIR_VERSION}/linux/*32.tar.gz ${PFX}/${DIR_VERSION}/all/${FILE_PFX}-${FILE_VERSION}-linux32.tar.gz
cp ${PFX}/${DIR_VERSION}/linux/*64.tar.gz ${PFX}/${DIR_VERSION}/all/${FILE_PFX}-${FILE_VERSION}-linux64.tar.gz

cp ${PFX}/${DIR_VERSION}/osx/*-osx-unsigned.dmg ${PFX}/${DIR_VERSION}/all/${FILE_PFX}-${FILE_VERSION}-osx.dmg
cp ${PFX}/${DIR_VERSION}/osx/*-osx-unsigned.tar.gz ${PFX}/${DIR_VERSION}/all/${FILE_PFX}-${FILE_VERSION}-osx.tar.gz
cp ${PFX}/${DIR_VERSION}/osx/*-osx64.tar.gz ${PFX}/${DIR_VERSION}/all/${FILE_PFX}-${FILE_VERSION}-osx64.tar.gz

cp ${PFX}/${DIR_VERSION}/win/*32-setup-unsigned.exe ${PFX}/${DIR_VERSION}/all/${FILE_PFX}-${FILE_VERSION}-win32-setup.exe
cp ${PFX}/${DIR_VERSION}/win/*64-setup-unsigned.exe ${PFX}/${DIR_VERSION}/all/${FILE_PFX}-${FILE_VERSION}-win64-setup.exe
cp ${PFX}/${DIR_VERSION}/win/*32.zip ${PFX}/${DIR_VERSION}/all/${FILE_PFX}-${FILE_VERSION}-win32.zip
cp ${PFX}/${DIR_VERSION}/win/*64.zip ${PFX}/${DIR_VERSION}/all/${FILE_PFX}-${FILE_VERSION}-win64.zip

echo "done"
echo "making signature file"
./sign.py ${VERSION} BCHunlimited
echo "done"
